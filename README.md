# README #

Choose any directory from the source, most names are self-explanatory. The instructions on how to compile, test and use each project will be in READMEs in their respective directories.

### What is this repository for? ###

* Repository of projects that showcases what I've programmed, for those interested in my code.

### Who do I talk to? ###

* Me, the writer/owner of this repo, Edmond Zalenski
* ezalen2 AT uic DOT edu