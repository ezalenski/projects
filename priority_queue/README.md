# Priority Queue #

This is a simple fairly clean implementation of a priority queue in C. It takes ID, priority pairs. IDs are allowed from 0 to n-1 where n is the fixed capacity of the queue which is set on creation.

## Simple Heap Sort Example 

```C
#include <stdio.h>
#include "pq.h"

int main() 
{
    int i;
    double val;
    PQ *q = pq_create(10, 1);
    for(i = 0, val = 1.0; (i < 10 && val > 0); i++, val -= .1) {
        pq_insert(q, i, val);
    }
    while(pq_size(q) > 0){ 
        pq_delete_top(q, &i, &val);
        printf("Min value %.2lf with id %d\n", val, i);
    }
    pq_free(q);
    return 0;
}
```

## Installation

To use, download the folder and `make pq.o`. can be linked to any source file like the example above. `gcc simple_heap_sort.c pq.o -o  heap_srt_ex`

## API Reference

| Functions                                                             | Description                                                                                                                                                                                                                                                                             |
| --------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `pq_create(int capacity, int min_heap)`                               | Returns: a pointer to a priority queue with the set capacity, and a min heap if `min_heap` is non-zero.                                                                                                                                                                                  |
| `pq_free(PQ * pq)`                                                    | Frees the priority queue.                                                                                                                                                                                                                                                               |
| `pq_insert(PQ * pq, int id, double priority)`                         | Returns: 1 on success, or 0 on failure. Fails if id is out of range or there is already an entry for id succeeds otherwise.                                                                                                                                                             |
| `pq_change_priority(PQ * pq, int id, double new_priority)`            | Returns: 1 on success, or 0 on failure. If there is an entry for the given id, its associated priority is changed to `new_priority` and the queue is modified accordingly. Otherwise, it is a failure (id not in pq or out of range)                                                    |
| `pq_remove_by_id(PQ * pq, int id)`                                    | Returns: 1 on success, or 0 on failure. If there is an entry associated with the given id, it is removed from queue. Otherwise the queue is unchanged and 0 is returned.                                                                                                                |
| `pq_get_priority(PQ * pq, int id, double *priority)`                  | Returns: 1 on success, or 0 on failure. If there is an entry for given id, priority is assigned the associated priority and 1 is returned. Otherwise 0 is returned and priority has no meaning.                                                                                         |
| `pq_delete_top(PQ * pq, int *id, double *priority)`                   | Returns: 1 on success, or 0 on failure (empty priority queue). If queue is non-empty the "top" element is deleted and its id and priority are stored in id and priority. The "top" element will be either min or max (wrt priority) depending on how the priority queue was configured. |
| `pq_size(PQ * pq)`                                                    | Returns: number of elements currently in queue.                                                                                                                                                                                                                                         |
| `pq_capacity(PQ * pq)`                                                | Returns: capacity of priority queue (as set on creation).                                                                                                                                                                                                                               |
| `pq_contains(PQ * pq, int id)`                                        | Returns: 1 if there is an entry in the queue for given id or 0 otherwise.                                                                                                                                                                                                               |



## Tests

To test any changes made to the priority queue, `make test` and run `./test`. There are a number of tests so only tests that fail are displayed. When a test fails, it prints the function that failed, the input, expected output, and the program's output. 
